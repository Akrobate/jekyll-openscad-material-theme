# jekyll-openscad-material-theme

## Descritpion

Template to reperesent an OpenScad project. Works with the python-comment-parser project. This template is an improvement of the first version and uses nos materializecss. https://materializecss.com version: materialize-v1.0.0

## Developping

To improve the template you can start it standalone. It includes some example ressources to get a very simple working project with 3 pages. 

You can start working with it launching the start.dev.sh script

```bash
sh start.dev.sh
```

## Manual build

The steps done by the start.dev.sh can be done manually
Please note that here we are using "jekyll" as name of the image tag, but you can specify any arbitrary name instead. Please upade all the commands if you change the default name

### Building the docker image

From the projects root folder you can launch the build command tagging your docker image

```bash
docker build -t jekyll .
```

### Installing gem packages

```bash
docker run --volume `pwd`:/public -it jekyll bash -c "bundler install"
```

### Starting dev server

```bash
docker run --volume `pwd`:/public -p 4000:4000 -it jekyll bash -c "bundler exec jekyll serve"
```

### Working inside the docker

```bash
docker run --volume `pwd`:/public -p 4000:4000 -it jekyll bash
```



