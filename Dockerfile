FROM ruby:2.6
COPY ./Gemfile ./
RUN gem install bundler jekyll
RUN bundle install
WORKDIR /public
