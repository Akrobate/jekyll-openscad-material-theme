---
layout: page
title: Test page 3
description: This an piece type and root is setted as false 
image: preview-test-page-3.png
type: piece
stl_link: preview-test-page-3.stl
root: false
related-pages:
    -
        title: Test page 2
        type: component
        url: test-page-2
        image: preview-test-page-2.png
    -
        title: Test page 1
        type: component
        url: test-page-1
        image: preview-test-page-1.png
---

```openscad
use <../envelopes/servo-arm-envelope.scad>
use <../assets/fonts/Freshman.ttf>

legPieceA();

/**
 * LegPieceA
 * @name LegPieceA
 * @description Hold servo arm to the leg
 * @type piece
 * @parent LegComponent
 * @stl
 */
module legPieceA(
    length = 40,
    width = 6
) {
    thickness = 3;

    hole_diameter = 6;

    _fn = 64;

    difference() {
        hull() {
            cylinder(h = thickness, r = width, center = false, $fn = _fn);
            translate([0, length, 0])
                cylinder(h = thickness, r = width, center = false, $fn = _fn);
        }

        translate([0, 0, 1.4])
            // scaling to prevent same size object
            scale([1.02, 1.02, 1])
            servoArmEnvelope(hole_h = 10, arm_h = 3);

        translate([0, length, 0])
            cylinder(h = thickness * 5, r = hole_diameter / 2, center = true, $fn = _fn);
    }
}
```